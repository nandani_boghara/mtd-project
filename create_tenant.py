# python manage.py shell

from tenant_app.models import Tenant, Domain

public_tenant = Tenant.objects.create(name='public', domain_url='public.com', schema_name='public')
# moweb_tenant = Tenant.objects.create(name='moweb', domain_url='moweb.com', schema_name='moweb')
surekha_tenant = Tenant.objects.create(name='surekha', domain_url='surekha.com', schema_name='surekha')
moweb2_tenant = Tenant.objects.create(name='moweb2', domain_url='moweb2.com', schema_name='moweb2')

public_domain = Domain.objects.create(domain_url='public.com', tenant=public_tenant)
# moweb_domain = Domain.objects.create(domain_url='moweb.com', tenant=moweb_tenant)
surekha_domain = Domain.objects.create(domain_url='surekha.com', tenant=surekha_tenant)
moweb2_domain = Domain.objects.create(domain_url='moweb2.com', tenant=moweb2_tenant)