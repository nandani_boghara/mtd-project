# mtd-project

```
python manage.py makemigrations tenant_app
python manage.py migrate_schemas --shared
```

Open create_tenant.py 

```
python manage.py shell
```
Copy all code from file and paste it in 'shell' and execute it.


Open create_admin.py 

Copy all code from file and paste it in 'shell' and execute it.


```
python manage.py makemigrations login_signup
python manage.py makemigrations details_app
python manage.py migrate_schemas
```