from django.urls import path
from .views import LoginView, LogoutView, SignupView
from rest_framework.authtoken import views

urlpatterns = [
    path('', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('signup/', SignupView.as_view(), name='signup'),
    path('api-token-auth/', views.obtain_auth_token, name='api-token-auth')
]
