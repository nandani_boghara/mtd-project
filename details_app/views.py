from django.shortcuts import render, redirect, get_object_or_404

# Create your views here.
from .serializers import EmployeeSerializer, CompanySerializer
from .models import Company, Employee
from rest_framework import generics
from rest_framework.authentication import SessionAuthentication, TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from django.db.models import Q


class CompanyListCreateAPIView(generics.ListCreateAPIView):
    queryset = Company.objects.all()
    serializer_class = CompanySerializer
    authentication_classes = [SessionAuthentication, TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        queryset = super().get_queryset()
        
        search_query = self.request.query_params.get('search', None)
        if search_query:
            queryset = queryset.filter(company_name__icontains=search_query)

        order_by = self.request.query_params.get('order_by', None)        
        if order_by:
            queryset = queryset.order_by(order_by)
        
        return queryset

class CompanyRetrieveUpdateDestroyAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Company.objects.all()
    serializer_class = CompanySerializer
    authentication_classes = [SessionAuthentication, TokenAuthentication]
    permission_classes = [IsAuthenticated]


class EmployeeListCreateAPIView(generics.ListCreateAPIView):
    queryset = Employee.objects.filter(is_active=True)
    serializer_class = EmployeeSerializer
    authentication_classes = [SessionAuthentication, TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        queryset = super().get_queryset()
        
        search_query = self.request.query_params.get('search', None)
        if search_query:
            queryset = queryset.filter(Q(first_name__icontains=search_query) | Q(last_name__icontains=search_query))

        order_by = self.request.query_params.get('order_by', None)        
        if order_by:
            queryset = queryset.order_by(order_by)
        
        return queryset


class EmployeeRetrieveUpdateDestroyAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Employee.objects.filter(is_active=True)
    serializer_class = EmployeeSerializer
    authentication_classes = [SessionAuthentication, TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def delete(self, request, *args, **kwargs):
        pk = kwargs.get('pk', None)
        employee = get_object_or_404(Employee.objects.filter(is_active=True), pk=pk)
        employee.is_active = False
        employee.save()
        return redirect('employee_detail')

