from rest_framework import serializers
from .models import Employee, Company





class EmployeeSerializer(serializers.ModelSerializer):
    tenant_detail = serializers.SerializerMethodField()


    class Meta:
        model = Employee
        fields = ['tenant_detail', 'employee_id', 'first_name', 'last_name', 'email_address', 'profile_pic', 'company']

    def get_tenant_detail(self, object):
        return 'Name: '+ str(object.tenant.name) + ', ID: ' + str(object.tenant.id)

    def create(self, validated_data):
        validated_data['tenant'] = self.context['request'].tenant
        return super().create(validated_data)


class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = '__all__'

