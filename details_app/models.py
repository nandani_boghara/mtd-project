from django.db import models

# Create your models here.
from django.contrib.auth import get_user_model
from PIL import Image
import uuid
from tenant_app.models import Tenant
import io
import os
from io import BytesIO
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.core.validators import validate_image_file_extension
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile


User = get_user_model()

class Company(models.Model):
    company_id = models.AutoField(primary_key=True)
    company_name = models.CharField(max_length=255, blank=False, null=False)

    def __str__(self):
        return self.company_name

def get_upload_to(instance, filename):
    # generate a unique filename using the uuid library
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    return filename


class Employee(models.Model):
    employee_id = models.AutoField(primary_key=True)
    tenant = models.ForeignKey(Tenant, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=255, blank=False, null=False)
    last_name = models.CharField(max_length=255, blank=False, null=False)
    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    email_address = models.EmailField(max_length=255, blank=True, null=True)
    profile_pic = models.FileField(upload_to=get_upload_to, blank=True, null=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.first_name + ' ' + self.last_name
