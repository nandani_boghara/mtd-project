# Generated by Django 4.1.7 on 2023-03-22 04:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('details_app', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='employee',
            name='profile_pic',
            field=models.ImageField(blank=True, null=True, upload_to='profile_pictures/'),
        ),
    ]
