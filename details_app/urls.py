from django.urls import path
from .views import CompanyListCreateAPIView, CompanyRetrieveUpdateDestroyAPIView, EmployeeListCreateAPIView, EmployeeRetrieveUpdateDestroyAPIView 

urlpatterns = [
    path('company/', CompanyListCreateAPIView.as_view(), name='company_list'),
    path('company/<int:pk>/', CompanyRetrieveUpdateDestroyAPIView.as_view(), name='company_detail'),
    path('employee/', EmployeeListCreateAPIView.as_view(), name='employee_list'),
    path('employee/<int:pk>/', EmployeeRetrieveUpdateDestroyAPIView.as_view(), name='employee_detail'),
]