from django.urls import path
from .views import TenantListCreateAPIView, TenantRetrieveUpdateDestroyAPIView, DomainListCreateAPIView, DomainRetrieveUpdateDestroyAPIView 

urlpatterns = [
    path('tenant/', TenantListCreateAPIView.as_view(), name='tenant_list'),
    path('tenant/<int:pk>/', TenantRetrieveUpdateDestroyAPIView.as_view(), name='tenant_detail'),
    path('domain/', DomainListCreateAPIView.as_view(), name='domain_list'),
    path('domain/<int:pk>/', DomainRetrieveUpdateDestroyAPIView.as_view(), name='domain_detail'),
]