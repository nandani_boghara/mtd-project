from django.db import models

# Create your models here.
from tenant_schemas.models import TenantMixin

class Tenant(TenantMixin):
    name = models.CharField(max_length=100, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    auto_create_schema = True

    def __str__(self):
        return self.name

class Domain(models.Model):
    tenant = models.ForeignKey(Tenant, on_delete=models.CASCADE)
    domain_url = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return self.domain_url