# python manage.py shell
from django.contrib.auth.models import User
from django.db import connection

connection.set_schema('surekha')
surekha_admin = User.objects.create_superuser(username='surekha_admin', email='admin@surekha.com', password='Admin123!@#')

connection.set_schema('moweb2')
moweb2_admin = User.objects.create_superuser(username='moweb2_admin', email='admin@moweb2.com', password='Admin123!@#')